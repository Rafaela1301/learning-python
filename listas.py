lista = ["p", "y", "t", "h", "o", "n"]
for item in lista:
    print (item)
"""
p
y
t
h
o
n
"""
print("AGORA COM WHILE")
count = 0
while count <= 5:
    print(count)
    count += 1
# 0 1 2 3 4 5
print("################################")
print("Tamanho da lista:", (len(lista)))

#range
range(4)
[0, 1, 2, 3]

range(2, 8)
[2, 3, 4, 5, 6, 7]

range(2, 13, 3)
[2, 5, 8, 11]

for i in range(20):
    print(i)