#concatenar string
valor ="triste"
print("hoje é um dia muito " + valor)
print("hoje é um dia muito " + "triste")
#transformando em string
valor2 = 3.14
print("O número pi é aproximadamente " + str(valor2))

print('There\'s a snake in my boot!')  # certo
# 'There's a snake in my boot!'   errado

# Métodos básicos de string
# função	descrição
# len()	    mostra o tamanho da string
# lower()	caixa baixa
# upper()	caixa alta
# str()	    converte em string
# isalpha()	retorna False se a string contiver algum caracter que não seja letras
# Exemplo, "flavio.upper()" equivale a FLAVIO.

nome ="Rafaela"

for letter in nome:
    print(letter)

"""
p
y
t
h
o
n
"""